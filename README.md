Overview
========
An OpenGL/Vulkan (Hopefully) sandbox.

Building
=======
Follow the standard CMake procedure:
* `mkdir build`
* `cmake ..`
    * For Ubuntu users with nVidia derivers, run `CMAKE_LIBRARY_PATH=/usr/lib/nvidia-<version #> cmake ..`)
    * You may need to install dependencies to get rid of some errors.
* `make`
