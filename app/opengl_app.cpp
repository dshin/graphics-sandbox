#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <array>
#include <thread>
#include <chrono>
#include <stdexcept>

#include <GL/gl3w.h>
#include "gl3w_context.hpp"

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include "glfw_opengl_context.hpp"

#include <assimp/Importer.hpp>
#include <boost/program_options.hpp>
#include <yaml-cpp/yaml.h>

#include "shader.hpp"
#include "program.hpp"

#include "perf_timer.hpp"

#include "vec.hpp"
#include "quat.hpp"
#include "conv.hpp"
#include "affine_matrix.hpp"

namespace
{

std::array<GLfloat, 12> verts{
    -0.5, -0.5, 0.5,
    0.5, -0.5, 0.5,
    0.5, 0.5, 0.5,
    -0.5, 0.5, 0.5};

std::array<GLushort, 6> idx{
    0, 1, 2, 0, 2, 3};

using std::cout;
using std::cerr;
using std::endl;
namespace po = boost::program_options;

}

int main(int argc, char *argv[])
{
    GLFWOpenGLContext glfwCtxt;

    std::string configPath{"./config/default.yaml"};

    po::options_description desc{"Graphics Sandbox"};
    desc.add_options()
        ("help", "Print this help message.")
        ("config,c", po::value<std::string>(), "Path to config file.");

    po::variables_map args;

    try
    {
        po::store(po::parse_command_line(argc, argv, desc), args);

        if (args.count("help"))
        {
            cout << desc << endl;
            return 0;
        }

        if (args.count("config"))
        { configPath = args["config"].as<std::string>(); }

        po::notify(args);
    }
    catch (po::error &e)
    {
        cerr << "Error: " << e.what() << endl << endl << desc << endl;
        return 1;
    }

    cout << "Loading config from " << configPath << endl;

    bool capped = false;
    unsigned swapInterval = 0;
    unsigned w, h, rate;
    try
    {
        YAML::Node config = YAML::LoadFile(configPath);

        if (config["framerate"].IsDefined())
        {
            capped = true;
            rate = config["framerate"].as<unsigned>();
        }

        if (config["vsync"].as<bool>())
        { swapInterval = 1; }

        w = config["width"].as<unsigned>();
        h = config["height"].as<unsigned>();
    }
    catch (YAML::Exception &e)
    {
        cerr << "Error while loading config: " << endl << e.what() << endl;
        return 1;
    }

    if (capped && rate == 0)
    { throw std::invalid_argument{"Zero is an invalid framerate."}; }

    cout << "Frame rate is ";
    if (capped)
    { cout << "capped @ " << rate << " Hz."; }
    else
    { cout << "uncapped."; }
    cout << endl;

    GLFWwindow *win = glfwCreateWindow(
        static_cast<int>(w),
        static_cast<int>(h),
        "Graphics Sandbox",
        nullptr,
        nullptr);

    glfwMakeContextCurrent(win);

    GL3WContext gl3wCtxt;

    cout << glGetString(GL_VERSION) << endl;

    std::ifstream vertStream{"./shader/basic.vert"};
    std::ifstream fragStream{"./shader/white.frag"};
    Program prog{
        Shader{vertStream, GL_VERTEX_SHADER},
        Shader{fragStream, GL_FRAGMENT_SHADER}};

    GLuint vao = 0;
    glGenVertexArrays(1, &vao);
    // From now on, all attributes bind to this vao.
    glBindVertexArray(vao);

    GLuint vertBuf;
    glGenBuffers(1, &vertBuf);
    glBindBuffer(GL_ARRAY_BUFFER, vertBuf);

    glBufferData(
        GL_ARRAY_BUFFER,
        sizeof(decltype(verts)::value_type) * verts.size(),
        verts.data(),
        GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0,
        3,
        GL_FLOAT,
        GL_FALSE,
        0,
        nullptr);

    GLuint idxBuf;
    glGenBuffers(1, &idxBuf);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idxBuf);

    glBufferData(
        GL_ELEMENT_ARRAY_BUFFER,
        sizeof(decltype(idx)::value_type) * idx.size(),
        idx.data(),
        GL_STATIC_DRAW);

    glUseProgram(prog.getHandle());
    GLint mvpLocation = glGetUniformLocation(prog.getHandle(), "modelViewProjectionMatrix");

    if (mvpLocation == -1)
    { throw std::runtime_error{"Uniform location not found."}; }

    const double targetFrameTime = [capped, rate] ()
    {
        if (!capped) { return 0.0; }
        return 1.0 / static_cast<double>(rate);
    } ();

    Vec3<float> pos{};
    Quat<float> ang{};
    AxisAngle<float> rot{{0, 0, 1}, 0.0};

    auto projection = affine_matrix::OrthogonalProjection<float>({0, 1.0}, {w, h}, 2.0);

    glViewport(0, 0, w, h);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    unsigned long ticks = 0;

    FPSCounter counter{0.1};

    glfwSwapInterval(swapInterval);

    cout.precision(5);

    double prevTotalTime = 0.0;

    while (!glfwWindowShouldClose(win))
    {
        Timer tickTimer;

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        rot.setAngle(rot.angle() + 1 * prevTotalTime);

        Mat4<float> model =
            affine_matrix::Translation(pos) *
            affine_matrix::Rotation(ang * conv::toQuat(rot));
        Mat4<float> modelProjection = projection * model;

        glUniformMatrix4fv(mvpLocation, 1, GL_TRUE, modelProjection.data());

        glDrawElements(
            GL_TRIANGLES,
            idx.size(),
            GL_UNSIGNED_SHORT,
            nullptr);

        glfwSwapBuffers(win);
        glfwPollEvents();
        const auto jobsTime = tickTimer.elapsed();
        if (jobsTime.count() < targetFrameTime)
        {
            std::this_thread::sleep_for(
                std::chrono::duration<double>{targetFrameTime} - jobsTime);
        }
        const auto totalTime = tickTimer.elapsed().count();
        prevTotalTime = totalTime;
        const auto [fpsAvailable, fps] = counter.update(ticks, totalTime);
        if (fpsAvailable)
        { cout << fps << " FPS" << endl; }

        ticks++;
    }

    return 0;
}
