#include <iostream>
#include <vector>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <assimp/Importer.hpp>
#include <boost/program_options.hpp>

namespace
{

using std::cout;
using std::cerr;
using std::endl;
namespace po = boost::program_options;

struct GLFWContext
{
    GLFWContext()
    {
        if (!glfwInit())
        { throw std::runtime_error{"Could not initialize glfw."}; }

        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    }

    ~GLFWContext()
    { glfwTerminate(); }
};

struct VulkanInstanceContext
{
    VulkanInstanceContext()
    {
        uint32_t count;
        const char ** exts = glfwGetRequiredInstanceExtensions(&count);
        if (exts == nullptr)
        { throw std::runtime_error("Could not get required Vulkan extensions for instance creation."); }

        const VkApplicationInfo appInfo{
            .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
            .pNext = nullptr,
            .pApplicationName = "vulkan_sandbox",
            .applicationVersion = 0,
            .pEngineName = "vulkan_sandbox",
            .engineVersion = 0,
            .apiVersion = VK_API_VERSION_1_0,
        };

        VkInstanceCreateInfo instCreateInfo{};
        instCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        instCreateInfo.pNext = nullptr;
        instCreateInfo.pApplicationInfo = &appInfo;
        instCreateInfo.enabledLayerCount = 0;
        instCreateInfo.ppEnabledLayerNames = nullptr;
        instCreateInfo.enabledExtensionCount = count;
        instCreateInfo.ppEnabledExtensionNames = exts;

        VkResult err = vkCreateInstance(&instCreateInfo, nullptr, &inst);
        if (err != VK_SUCCESS)
        { throw std::runtime_error("Vulkan create instance failed: " + std::to_string(err)); }
    }

    ~VulkanInstanceContext()
    {
        vkDestroyInstance(inst, nullptr);
    }

    VkInstance inst;
};

struct VulkanSurfaceContext
{
    VulkanSurfaceContext(const VulkanInstanceContext &inst, GLFWwindow *win) :
        inst{inst}
    {
        VkResult err = glfwCreateWindowSurface(inst.inst, win, nullptr, &surf);
        if (err != VK_SUCCESS)
        { throw std::runtime_error("Failed to create window surface: " + std::to_string(err)); }
    }

    ~VulkanSurfaceContext()
    {
        vkDestroySurfaceKHR(inst.inst, surf, nullptr);
    }

    const VulkanInstanceContext &inst;
    VkSurfaceKHR surf;
};

}

int main(int argc, char *argv[])
{
    GLFWContext glfwCtxt;

    unsigned w, h;

    po::options_description desc{"Graphics Sandbox"};
    desc.add_options()
        ("help", "Print this help message.")
        ("width,w", po::value<unsigned>(&w)->required(), "Specify window width.")
        ("height,h", po::value<unsigned>(&h)->required(), "Specify window height.");

    po::variables_map args;

    try
    {
        po::store(po::parse_command_line(argc, argv, desc), args);

        if (args.count("help"))
        {
            cout << desc << endl;
            return 0;
        }

        po::notify(args);
    }
    catch (po::error &e)
    {
        cerr << "Error: " << e.what() << endl << endl << desc << endl;
        return 1;
    }

    GLFWwindow *win = glfwCreateWindow(
        static_cast<int>(w),
        static_cast<int>(h),
        "Graphics Sandbox",
        nullptr,
        nullptr);

    VulkanInstanceContext vk;
    VulkanSurfaceContext surf{vk, win};

    while (!glfwWindowShouldClose(win))
    {
        glfwSwapBuffers(win);
        glfwPollEvents();
    }

    return 0;
}
