#include <gtest/gtest.h>

#include "affine_matrix.hpp"
#include "axis_angle.hpp"

TEST(AffineMatrix, Translation)
{
    Mat4<float> mat = affine_matrix::Translation<float>(-1.0, 2.0, -3.0);

    for (unsigned i = 0; i < 3; i++)
    {
        for (unsigned j = 0; j < 3; j++)
        {
            if (i == j)
            { EXPECT_FLOAT_EQ(mat(i, j), 1); }
            else
            { EXPECT_FLOAT_EQ(mat(i, j), 0); }
        }
    }

    EXPECT_FLOAT_EQ(mat(0, 3), -1.0);
    EXPECT_FLOAT_EQ(mat(1, 3), 2.0);
    EXPECT_FLOAT_EQ(mat(2, 3), -3.0);
    EXPECT_FLOAT_EQ(mat(3, 3), 1.0);

}

TEST(AffineMatrix, Scale)
{
    Mat4<float> mat = affine_matrix::Scale<float>(2.0, 3.0, 4.0);

    for (unsigned i = 0; i < 3; i++)
    {
        for (unsigned j = 0; j < 3; j++)
        {
            if (i != j)
            { EXPECT_FLOAT_EQ(mat(i, j), 0); }
        }
    }

    EXPECT_FLOAT_EQ(mat(0, 0), 2.0);
    EXPECT_FLOAT_EQ(mat(1, 1), 3.0);
    EXPECT_FLOAT_EQ(mat(2, 2), 4.0);
    EXPECT_FLOAT_EQ(mat(3, 3), 1.0);
}

TEST(AffineMatrix, RotationTrivial)
{
    constexpr float rot = 0.3;

    Mat4<float> mat = affine_matrix::Rotation<float>(AxisAngle{{1, 0, 0}, rot});

    const float cosT = std::cos(rot);
    const float sinT = std::sin(rot);

    EXPECT_FLOAT_EQ(mat(0, 0), 1.0);
    EXPECT_FLOAT_EQ(mat(0, 1), 0.0);
    EXPECT_FLOAT_EQ(mat(0, 2), 0.0);

    EXPECT_FLOAT_EQ(mat(1, 0), 0.0);
    EXPECT_FLOAT_EQ(mat(1, 1), cosT);
    EXPECT_FLOAT_EQ(mat(1, 2), -sinT);

    EXPECT_FLOAT_EQ(mat(2, 0), 0.0);
    EXPECT_FLOAT_EQ(mat(2, 1), sinT);
    EXPECT_FLOAT_EQ(mat(2, 2), cosT);

    EXPECT_FLOAT_EQ(mat(0, 3), 0.0);
    EXPECT_FLOAT_EQ(mat(1, 3), 0.0);
    EXPECT_FLOAT_EQ(mat(2, 3), 0.0);
    EXPECT_FLOAT_EQ(mat(3, 3), 1.0);

    mat = affine_matrix::Rotation<float>(AxisAngle{{0, 1, 0}, rot});

    EXPECT_FLOAT_EQ(mat(0, 0), cosT);
    EXPECT_FLOAT_EQ(mat(0, 1), 0.0);
    EXPECT_FLOAT_EQ(mat(0, 2), sinT);

    EXPECT_FLOAT_EQ(mat(1, 0), 0.0);
    EXPECT_FLOAT_EQ(mat(1, 1), 1.0);
    EXPECT_FLOAT_EQ(mat(1, 2), 0.0);

    EXPECT_FLOAT_EQ(mat(2, 0), -sinT);
    EXPECT_FLOAT_EQ(mat(2, 1), 0.0);
    EXPECT_FLOAT_EQ(mat(2, 2), cosT);

    EXPECT_FLOAT_EQ(mat(0, 3), 0.0);
    EXPECT_FLOAT_EQ(mat(1, 3), 0.0);
    EXPECT_FLOAT_EQ(mat(2, 3), 0.0);
    EXPECT_FLOAT_EQ(mat(3, 3), 1.0);

    mat = affine_matrix::Rotation<float>(AxisAngle{{0, 0, 1}, rot});

    EXPECT_FLOAT_EQ(mat(0, 0), cosT);
    EXPECT_FLOAT_EQ(mat(0, 1), -sinT);
    EXPECT_FLOAT_EQ(mat(0, 2), 0.0);

    EXPECT_FLOAT_EQ(mat(1, 0), sinT);
    EXPECT_FLOAT_EQ(mat(1, 1), cosT);
    EXPECT_FLOAT_EQ(mat(1, 2), 0.0);

    EXPECT_FLOAT_EQ(mat(2, 0), 0.0);
    EXPECT_FLOAT_EQ(mat(2, 1), 0.0);
    EXPECT_FLOAT_EQ(mat(2, 2), 1.0);

    EXPECT_FLOAT_EQ(mat(0, 3), 0.0);
    EXPECT_FLOAT_EQ(mat(1, 3), 0.0);
    EXPECT_FLOAT_EQ(mat(2, 3), 0.0);
    EXPECT_FLOAT_EQ(mat(3, 3), 1.0);
}

// TODO: Add more
