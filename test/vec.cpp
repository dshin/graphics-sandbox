#include <gtest/gtest.h>

#include "vec.hpp"

TEST(Vec, Construction)
{
    Vec<float, 2> vec2{0.1, 1.2};
    Vec<float, 3> vec3{0.2, 1.3, 2.4};
    Vec<float, 4> vec4{0.3, 1.4, 2.5, 3.6};

    EXPECT_FLOAT_EQ(vec2.x(), 0.1);
    EXPECT_FLOAT_EQ(vec2.y(), 1.2);

    EXPECT_FLOAT_EQ(vec3.x(), 0.2);
    EXPECT_FLOAT_EQ(vec3.y(), 1.3);
    EXPECT_FLOAT_EQ(vec3.z(), 2.4);

    EXPECT_FLOAT_EQ(vec4.x(), 0.3);
    EXPECT_FLOAT_EQ(vec4.y(), 1.4);
    EXPECT_FLOAT_EQ(vec4.z(), 2.5);
    EXPECT_FLOAT_EQ(vec4.w(), 3.6);
}

TEST(Vec, Mutation)
{
    Vec<float, 4> vec1{1.0, 3.5, -2.1, 8.3};
    Vec<float, 4> vec2;

    vec2.x() = vec1.w();
    vec2.y() = vec1.z();
    vec2.z() = vec1.y();
    vec2.w() = vec1.x();

    EXPECT_FLOAT_EQ(vec2.x(), vec1.w());
    EXPECT_FLOAT_EQ(vec2.y(), vec1.z());
    EXPECT_FLOAT_EQ(vec2.z(), vec1.y());
    EXPECT_FLOAT_EQ(vec2.w(), vec1.x());
}

TEST(Vec, Negation)
{
    Vec<float, 4> vec1{1.2, 3.4, 5.6, 6.7};
    auto vec2 = -vec1;

    EXPECT_FLOAT_EQ(vec2.x(), -vec1.x());
    EXPECT_FLOAT_EQ(vec2.y(), -vec1.y());
    EXPECT_FLOAT_EQ(vec2.z(), -vec1.z());
    EXPECT_FLOAT_EQ(vec2.w(), -vec1.w());
}

TEST(Vec, ScalarMultiplication)
{
    Vec<float, 2> vecOrig{1.2, 3.4};
    const float mult = -3.2;

    auto vec1 = mult * vecOrig;
    auto vec2 = vecOrig * mult;
    auto vec3 = vecOrig / mult;

    EXPECT_FLOAT_EQ(vec1.x(), vecOrig.x() * mult);
    EXPECT_FLOAT_EQ(vec1.y(), vecOrig.y() * mult);

    EXPECT_FLOAT_EQ(vec2.x(), vecOrig.x() * mult);
    EXPECT_FLOAT_EQ(vec2.y(), vecOrig.y() * mult);

    EXPECT_FLOAT_EQ(vec3.x(), vecOrig.x() / mult);
    EXPECT_FLOAT_EQ(vec3.y(), vecOrig.y() / mult);
}

TEST(Vec, Dot)
{
    Vec<float, 4> vec1{1.0, 2.0, 3.0, 4.0};
    Vec<float, 4> vec2{1.0, -2.0, 3.0, -4.0};

    const auto result1 = vec1.Dot(vec2);
    const auto result2 = vec2.Dot(vec1);

    const float expectedResult =
        vec1.x() * vec2.x() +
        vec1.y() * vec2.y() +
        vec1.z() * vec2.z() +
        vec1.w() * vec2.w();
    EXPECT_FLOAT_EQ(result1, expectedResult);
    EXPECT_FLOAT_EQ(result2, expectedResult);
}

TEST(Vec, Length)
{
    Vec<float, 4> vec{1.0, 2.0, 3.0, 4.0};
    const float expectedSq =
        vec.x() * vec.x() +
        vec.y() * vec.y() +
        vec.z() * vec.z() +
        vec.w() * vec.w();

    EXPECT_FLOAT_EQ(vec.Length2(), expectedSq);
    EXPECT_FLOAT_EQ(vec.Length(), std::sqrt(expectedSq));
}

TEST(Vec, Unit)
{
    Vec<float, 4> vec{1.0, 2.0, 3.0, 4.0};
    const auto prevLength = vec.Length();

    auto unit = vec.Unit();
    EXPECT_FLOAT_EQ(unit.Length(), 1.0);
    EXPECT_FLOAT_EQ(unit.x(), vec.x() / prevLength);
    EXPECT_FLOAT_EQ(unit.y(), vec.y() / prevLength);
    EXPECT_FLOAT_EQ(unit.z(), vec.z() / prevLength);
    EXPECT_FLOAT_EQ(unit.w(), vec.w() / prevLength);
}

TEST(Vec, Cross)
{
    Vec<float, 3> vec1{1.0, 2.0, 3.0};
    Vec<float, 3> vec2{3.0, -4.0, 5.0};

    auto res = vec1.Cross(vec2);

    EXPECT_FLOAT_EQ(res.x(), 22);
    EXPECT_FLOAT_EQ(res.y(), 4);
    EXPECT_FLOAT_EQ(res.z(), -10);
}

TEST(Vec, Extension3D)
{
    Vec<float, 2> vec1{1.0, 2.0};
    Vec<float, 3> vec2{vec1.x(), vec1.y(), 0.0};

    Vec<float, 3> vec3 = vec1.ExtendTo3D();

    EXPECT_FLOAT_EQ(vec3.x(), vec2.x());
    EXPECT_FLOAT_EQ(vec3.y(), vec2.y());
    EXPECT_FLOAT_EQ(vec3.z(), vec2.z());
}

TEST(Vec, Extension2DTo4D)
{
    Vec<float, 2> vec1{1.0, 2.0};
    Vec<float, 4> vec2{vec1.x(), vec1.y(), 0.0, 0.0};

    Vec<float, 4> vec3 = vec1.ExtendTo4D();

    EXPECT_FLOAT_EQ(vec3.x(), vec2.x());
    EXPECT_FLOAT_EQ(vec3.y(), vec2.y());
    EXPECT_FLOAT_EQ(vec3.z(), vec2.z());
    EXPECT_FLOAT_EQ(vec3.w(), vec2.w());
}

TEST(Vec, Extension3DTo4D)
{
    Vec<float, 3> vec1{1.0, 2.0, 3.0};
    Vec<float, 4> vec2{vec1.x(), vec1.y(), vec1.z(), 0.0};

    Vec<float, 4> vec3 = vec1.ExtendTo4D();

    EXPECT_FLOAT_EQ(vec3.x(), vec2.x());
    EXPECT_FLOAT_EQ(vec3.y(), vec2.y());
    EXPECT_FLOAT_EQ(vec3.z(), vec2.z());
    EXPECT_FLOAT_EQ(vec3.w(), vec2.w());
}

