#include <gtest/gtest.h>

#include "quat.hpp"
#include "vec.hpp"

TEST(Quat, Construction)
{
    Quat<float> q1{};
    EXPECT_FLOAT_EQ(q1.x(), 0);
    EXPECT_FLOAT_EQ(q1.y(), 0);
    EXPECT_FLOAT_EQ(q1.z(), 0);
    EXPECT_FLOAT_EQ(q1.w(), 1);

    Vec4<float> v{1, -2, 3, -4};
    Quat<float> q2{v};
    EXPECT_FLOAT_EQ(q2.x(), v.x());
    EXPECT_FLOAT_EQ(q2.y(), v.y());
    EXPECT_FLOAT_EQ(q2.z(), v.z());
    EXPECT_FLOAT_EQ(q2.w(), v.w());

    Quat<float> q3{v.x(), v.y(), v.z(), v.w()};
    EXPECT_FLOAT_EQ(q3.x(), v.x());
    EXPECT_FLOAT_EQ(q3.y(), v.y());
    EXPECT_FLOAT_EQ(q3.z(), v.z());
    EXPECT_FLOAT_EQ(q3.w(), v.w());
}

TEST(Quat, Conjugate)
{
    Quat<float> q1{1, -2, 3, -4};

    Quat<float> q2 = q1.Conj();
    EXPECT_FLOAT_EQ(q2.x(), -q1.x());
    EXPECT_FLOAT_EQ(q2.y(), -q1.y());
    EXPECT_FLOAT_EQ(q2.z(), -q1.z());
    EXPECT_FLOAT_EQ(q2.w(), q1.w());
}

TEST(Quat, MultiplicationIdentity)
{
    Quat<float> q1{};
    Quat<float> q2{};

    Quat<float> q3 = q1 * q2;

    EXPECT_FLOAT_EQ(q3.x(), 0);
    EXPECT_FLOAT_EQ(q3.y(), 0);
    EXPECT_FLOAT_EQ(q3.z(), 0);
    EXPECT_FLOAT_EQ(q3.w(), 1);
}

TEST(Quat, MultiplicationTrivial)
{
    Quat<float> q1{1.0, 0, 0, 0};
    Quat<float> q2{-1.0, 0, 0, 0};

    Quat<float> q3 = q1 * q2;

    EXPECT_FLOAT_EQ(q3.x(), 0);
    EXPECT_FLOAT_EQ(q3.y(), 0);
    EXPECT_FLOAT_EQ(q3.z(), 0);
    EXPECT_FLOAT_EQ(q3.w(), 1);

    Quat<float> q4{1.0, 0, 0, 0};
    Quat<float> q5{0, -1.0, 0, 0};

    Quat<float> q6 = q4 * q5;

    EXPECT_FLOAT_EQ(q6.x(), 0);
    EXPECT_FLOAT_EQ(q6.y(), 0);
    EXPECT_FLOAT_EQ(q6.z(), -1);
    EXPECT_FLOAT_EQ(q6.w(), 0);
}

TEST(Quat, Multiplication)
{
    Quat<float> q1{-3.4, 5.6, -7.8, 1.2};
    Quat<float> q2{6.5, -4.3, 2.1, -8.7};

    Quat<float> q3 = q1 * q2;

    EXPECT_FLOAT_EQ(q3.x(), 15.599998);
    EXPECT_FLOAT_EQ(q3.y(), -97.44);
    EXPECT_FLOAT_EQ(q3.z(), 48.6);
    EXPECT_FLOAT_EQ(q3.w(), 52.119995);
}
