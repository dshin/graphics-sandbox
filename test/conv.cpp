#include <gtest/gtest.h>

#include "conv.hpp"

TEST(Conversion, Identity)
{
    AxisAngle<float> axisAngle1{{1, 2, 3}, 0.5};
    auto quat = conv::toQuat(axisAngle1);
    auto axisAngle2 = conv::toAxisAngle(quat);

    EXPECT_FLOAT_EQ(axisAngle2.axis().x(), axisAngle1.axis().x());
    EXPECT_FLOAT_EQ(axisAngle2.axis().y(), axisAngle1.axis().y());
    EXPECT_FLOAT_EQ(axisAngle2.axis().z(), axisAngle1.axis().z());
}

TEST(AxisAngleToQuat, Trivial)
{
    AxisAngle<float> axisAngle1{{1, 0, 0}, 0};
    auto quat1 = conv::toQuat(axisAngle1);

    EXPECT_FLOAT_EQ(quat1.x(), 0);
    EXPECT_FLOAT_EQ(quat1.y(), 0);
    EXPECT_FLOAT_EQ(quat1.z(), 0);
    EXPECT_FLOAT_EQ(quat1.w(), 1);
}

TEST(QuatToAxisAngle, Trivial)
{
    // TODO
}

// TODO: More cases
