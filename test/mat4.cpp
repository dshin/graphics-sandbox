#include <gtest/gtest.h>

#include "mat4.hpp"

namespace
{
constexpr size_t rows = 4;
constexpr size_t cols = 4;
}

TEST(Mat4, ZeroConstruction)
{
    Mat4<float> mat;

    for (unsigned row = 0; row < rows; row++)
    {
        for (unsigned col = 0; col < cols; col++)
        { EXPECT_FLOAT_EQ(mat(row, col), 0); }
    }
}

TEST(Mat4, Construction)
{
    Mat4<float> mat{
        {0, 1, 2, 3},
        {4, 5, 6, 7},
        {8, 9, 10, 11},
        {12, 13, 14, 15}};

    for (unsigned row = 0; row < rows; row++)
    {
        for (unsigned col = 0; col < cols; col++)
        {
            EXPECT_FLOAT_EQ(mat(row, col), row * rows + col);
        }
    }
}

TEST(Mat4, IdentityConstruction)
{
    Mat4<float> mat = Mat4<float>::Identity();

    for (unsigned row = 0; row < rows; row++)
    {
        for (unsigned col = 0; col < cols; col++)
        {
            if (row != col)
            { EXPECT_FLOAT_EQ(mat(row, col), 0); }
            if (row == col)
            { EXPECT_FLOAT_EQ(mat(row, col), 1); }
        }
    }
}

TEST(Mat4, Mutation)
{
    Mat4<float> mat;

    for (unsigned row = 0; row < rows; row++)
    {
        for (unsigned col = 0; col < cols; col++)
        {
            mat(row, col) = row * rows + col;
        }
    }

    for (unsigned row = 0; row < rows; row++)
    {
        for (unsigned col = 0; col < cols; col++)
        {
            EXPECT_FLOAT_EQ(mat(row, col), row * rows + col);
        }
    }
}

TEST(Mat4, Copy)
{
    Mat4<float> mat{
        {0, 1, 2, 3},
        {4, 5, 6, 7},
        {8, 9, 10, 11},
        {12, 13, 14, 15}};

    Mat4<float> mat1 = mat;
    Mat4<float> mat2{mat};

    for (unsigned row = 0; row < rows; row++)
    {
        for (unsigned col = 0; col < cols; col++)
        {
            EXPECT_FLOAT_EQ(mat1(row, col), mat(row, col));
            EXPECT_FLOAT_EQ(mat2(row, col), mat(row, col));
        }
    }
}

TEST(Mat4, Mult)
{
    Mat4<float> mat1{
        {11, 2, 7, -11},
        {-5, 1, -7, -10},
        {3, 8, -15, -12},
        {7, 14, 13, 8}};

    Mat4<float> mat2{
        {-15, -13, -14, 1},
        {-10, 8, 1, 3},
        {1, 11, -3, -9},
        {-9, 11, 0, 9}};

    Mat4<float> res = mat1 * mat2;

    Mat4<float> mat3{
        {-79, -171, -173, -145},
        {148, -114, 92, -29},
        {-32, -272, 11, 54},
        {-304, 252, -123, 4}};

    for (unsigned row = 0; row < rows; row++)
    {
        for (unsigned col = 0; col < cols; col++)
        {
            EXPECT_FLOAT_EQ(res(row, col), mat3(row, col));
        }
    }
}
