#include <gtest/gtest.h>

#include "angle.hpp"
#include "axis_angle.hpp"

TEST(AxisAngle, Construction)
{
    Vec3<float> axis1{1, 2, 3};
    Vec3<float> axis2{1, 3, -2};
    Vec3<float> axis3{-3, -2, 1};
    AxisAngle<float> axisAngle{axis1, 0.0};

    EXPECT_FLOAT_EQ(axisAngle.angle(), 0.0);
    EXPECT_FLOAT_EQ(axisAngle.axis().x(), axis1.Unit().x());
    EXPECT_FLOAT_EQ(axisAngle.axis().y(), axis1.Unit().y());
    EXPECT_FLOAT_EQ(axisAngle.axis().z(), axis1.Unit().z());

    axisAngle.setAxis(axis2);
    axisAngle.setAngle(1.2);

    EXPECT_FLOAT_EQ(axisAngle.angle(), 1.2);
    EXPECT_FLOAT_EQ(axisAngle.axis().x(), axis2.Unit().x());
    EXPECT_FLOAT_EQ(axisAngle.axis().y(), axis2.Unit().y());
    EXPECT_FLOAT_EQ(axisAngle.axis().z(), axis2.Unit().z());

    axisAngle.setAxis(axis3);
    axisAngle.setAngle(-3.1);

    EXPECT_FLOAT_EQ(axisAngle.angle(), -3.1);
    EXPECT_FLOAT_EQ(axisAngle.axis().x(), axis3.Unit().x());
    EXPECT_FLOAT_EQ(axisAngle.axis().y(), axis3.Unit().y());
    EXPECT_FLOAT_EQ(axisAngle.axis().z(), axis3.Unit().z());
}

TEST(AxisAngle, Wrapping)
{
    Vec3<float> axis{1, 0, 0};
    AxisAngle<float> axisAngle{axis, angle::pi<float>() + .1};

    EXPECT_FLOAT_EQ(axisAngle.angle(), -angle::pi<float>() + 0.1);

    axisAngle.setAngle(-angle::pi<float>() - .1);
    EXPECT_FLOAT_EQ(axisAngle.angle(), angle::pi<float>() - 0.1);

    axisAngle.setAngle(angle::pi<float>() - .1);
    EXPECT_FLOAT_EQ(axisAngle.angle(), angle::pi<float>() - 0.1);

    axisAngle.setAngle(-angle::pi<float>() + .1);
    EXPECT_FLOAT_EQ(axisAngle.angle(), -angle::pi<float>() + 0.1);
}
