#include "program.hpp"
#include <stdexcept>
#include <vector>

void ProgramDeleter::operator()(GLuint handle) const
{ glDeleteProgram(handle); }

Program::Program(
    const Shader &vertShader,
    const Shader &fragShader) :
    Handle{glCreateProgram()}
{
    glAttachShader(handle(), vertShader.getHandle());
    glAttachShader(handle(), fragShader.getHandle());
    glLinkProgram(handle());

    GLint linked = GL_FALSE;
    glGetProgramiv(handle(), GL_LINK_STATUS, &linked);
    if (linked == GL_FALSE)
    {
		GLint logLength = 0;
		glGetProgramiv(handle(), GL_INFO_LOG_LENGTH, &logLength);

		std::vector<GLchar> infoLog(logLength);
		glGetProgramInfoLog(
			handle(),
            logLength,
            &logLength,
            &infoLog[0]);

        throw std::runtime_error(
            "Failed to link: " +
            std::string(infoLog.begin(), infoLog.end()));
    }
}
