#include "glfw_opengl_context.hpp"

#include "platform_gl.hpp"
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

namespace
{

void HintVersion(const unsigned major, const unsigned minor)
{
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, static_cast<int>(major));
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, static_cast<int>(minor));
}

}

GLFWOpenGLContext::GLFWOpenGLContext()
{
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    HintVersion(3, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
}

GLFWOpenGLContext::GLFWOpenGLContext(const unsigned major, const unsigned minor)
{
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    HintVersion(major, minor);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
}
