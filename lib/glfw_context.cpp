#include "glfw_context.hpp"

#include <stdexcept>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

GLFWContext::GLFWContext()
{
    if (!glfwInit())
    { throw std::runtime_error{"Could not initialize glfw."}; }
}

GLFWContext::~GLFWContext()
{ glfwTerminate(); }
