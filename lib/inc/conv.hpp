#ifndef CONV_HPP_
#define CONV_HPP_

#include <cmath>

#include "quat.hpp"
#include "axis_angle.hpp"
#include "float_eq.hpp"

namespace conv
{

template <typename T, typename EqualsFunc = FloatEqualsFuncDefault<T>>
AxisAngle<T> toAxisAngle(const Quat<T> &quat, EqualsFunc equals = {})
{
    const T div = std::sqrt(1.0 - std::pow(quat.w(), 2));
    if (equals(div, 0)) { return {}; }
    return {
        {quat.x() / div, quat.y() / div, quat.z() / div},
        static_cast<T>(2.0 * std::acos(quat.w()))};
}

template <typename T>
Quat<T> toQuat(const AxisAngle<T> &aa)
{
    const T angHalf = aa.angle() / 2;
    const T mult = std::sin(angHalf);
    return {
        aa.axis().x() * mult,
        aa.axis().y() * mult,
        aa.axis().z() * mult,
        std::cos(angHalf)};
}

}

#endif
