#ifndef ANGLE_HPP_
#define ANGLE_HPP_

#define _USE_MATH_DEFINES
#include <cmath>

namespace angle
{

template <typename T>
constexpr T pi()
{ return static_cast<T>(M_PI); }

template <typename T>
T wrap(const T v)
{
    T wrapped = std::fmod(v + pi<T>(), 2.0 * pi<T>());
    if (wrapped < 0) { wrapped += 2.0 * pi<T>(); }
    return wrapped - pi<T>();
}

}

#endif
