#ifndef SHADER_HPP_
#define SHADER_HPP_

#include "platform_gl.hpp"
#include <iostream>

#include "handle.hpp"

struct ShaderDeleter
{
    void operator()(GLuint handle) const;
};

class Shader : public Handle<GLuint, 0, ShaderDeleter>
{
public:
    Shader(std::istream &stream, const GLenum type);

    Handle::Type getHandle() const { return handle(); }
};

#endif
