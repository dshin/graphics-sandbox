#ifndef AXIS_ANGLE_HPP_
#define AXIS_ANGLE_HPP_

#include <angle.hpp>
#include <vec.hpp>

template <typename T>
class AxisAngle
{
public:
    static_assert(std::numeric_limits<T>::is_iec559, "Floating point required.");

    AxisAngle() : vec{}, ang{} {}

    AxisAngle(const Vec3<T> axis, const T angle) :
        vec{axis.Unit()},
        ang{angle::wrap(angle)}
    {}

    Vec3<T> axis() const { return vec; }
    void setAxis(const Vec3<T> &axis) { vec = axis.Unit(); }
    T angle() const { return ang; }
    void setAngle(const T angle) { ang = angle::wrap(angle); }
private:

    Vec3<T> vec;
    T ang;
};

#endif
