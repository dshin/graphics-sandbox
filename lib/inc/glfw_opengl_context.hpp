#ifndef GLFW_OPENGL_CONTEXT_HPP_
#define GLFW_OPENGL_CONTEXT_HPP_

#include "glfw_context.hpp"

struct GLFWOpenGLContext : public GLFWContext
{
    GLFWOpenGLContext();
    GLFWOpenGLContext(const unsigned major, const unsigned minor);
};

#endif
