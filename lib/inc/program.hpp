#ifndef PROGRAM_HPP_
#define PROGRAM_HPP_

#include "platform_gl.hpp"

#include "shader.hpp"
#include "handle.hpp"

struct ProgramDeleter
{
    void operator()(GLuint handle) const;
};

class Program : public Handle<GLuint, 0, ProgramDeleter>
{
public:
    Program(const Shader &vertShader, const Shader &fragShader);

    GLuint getHandle() const { return handle(); }
};

#endif
