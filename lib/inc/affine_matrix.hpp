#ifndef AFFINE_MATRIX_HPP_
#define AFFINE_MATRIX_HPP_

#include <cmath>
#include <utility>

#include "vec.hpp"
#include "mat4.hpp"
#include "axis_angle.hpp"
#include "quat.hpp"
#include "conv.hpp"

namespace affine_matrix
{

template <typename T>
Mat4<T> Translation(const T x, const T y, const T z)
{
    auto res = Mat4<T>::Identity();
    res(0, 3) = x;
    res(1, 3) = y;
    res(2, 3) = z;

    return std::move(res);
}
template <typename T>
Mat4<T> Translation(const Vec3<T> &vec)
{
    return std::move(Translation(vec.x(), vec.y(), vec.z()));
}

template <typename T>
Mat4<T> Scale(const T x, const T y, const T z)
{
    auto res = Mat4<T>::Identity();
    res(0, 0) = x;
    res(1, 1) = y;
    res(2, 2) = z;

    return std::move(res);
}

template <typename T>
Mat4<T> Rotation(const AxisAngle<T> &aa)
{
    const T x = aa.axis().x();
    const T y = aa.axis().y();
    const T z = aa.axis().z();
    const T ang = aa.angle();
    const T sinT = std::sin(ang);
    const T cosT = std::cos(ang);
    const T comm = 1.0 - cosT;
    const T aboveAntiDiag1 = x * y * comm;
    const T aboveAntiDiag2 = z * sinT;
    const T antiDiagCorner1 = x * z * comm;
    const T antiDiagCorner2 = y * sinT;
    const T belowAntiDiag1 = y * z * comm;
    const T belowAntiDiag2 = x * sinT;

    auto res = Mat4<T>::Identity();
    res(0, 0) = cosT + std::pow(x, 2) * comm;
    res(0, 1) = aboveAntiDiag1 - aboveAntiDiag2;
    res(0, 2) = antiDiagCorner1 + antiDiagCorner2;
    res(1, 0) = aboveAntiDiag1 + aboveAntiDiag2;
    res(1, 1) = cosT + std::pow(y, 2) * comm;
    res(1, 2) = belowAntiDiag1 - belowAntiDiag2;
    res(2, 0) = antiDiagCorner1 - antiDiagCorner2;
    res(2, 1) = belowAntiDiag1 + belowAntiDiag2;
    res(2, 2) = cosT + std::pow(z, 2) * comm;

    return std::move(res);
}

template <typename T>
Mat4<T> Rotation(const Quat<T> &quat)
{
    return std::move(Rotation(conv::toAxisAngle(quat)));
}

template <typename T>
Mat4<T> OrthogonalProjection(const std::pair<T, T> nearFar, const std::pair<T, T> wh)
{
    auto res = Mat4<T>::Identity();

    const double farNearDiff = nearFar.second - nearFar.first;

    res(0, 0) = 2.0 / wh.first;
    res(1, 1) = 2.0 / wh.second;
    res(2, 2) = 2.0 / farNearDiff;
    res(2, 3) = -(nearFar.first + nearFar.second) / farNearDiff;

    return std::move(res);
}

template <typename T>
Mat4<T> OrthogonalProjection(const std::pair<T, T> nearFar, const std::pair<T, T> wh, const T min)
{
    auto newWh = [wh, min] () -> std::pair<T, T>
    {
        const T aspectRatio = static_cast<T>(wh.first) / wh.second;

        if (aspectRatio < 1.0)
        { return {min, min / aspectRatio}; }
        return {min * aspectRatio, min};
    } ();

    return affine_matrix::OrthogonalProjection(nearFar, newWh);
}

}

#endif
