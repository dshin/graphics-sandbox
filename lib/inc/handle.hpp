#ifndef HANDLE_HPP_
#define HANDLE_HPP_

#include <stdexcept>
#include <functional>

template <class HandleType, HandleType invalid, typename ReleaseFunc>
class Handle
{
public:
    using Type = HandleType;
    using InvalidHandle = std::runtime_error;
    Handle(const HandleType h) : h{h}
    {
        if (h == invalid)
        { throw InvalidHandle{"Provided handle is invalid."}; }
    }

    Handle() = delete;
    Handle(const Handle &other) = delete;
    Handle(Handle &&other) noexcept :
        h{other.h}
    { other.h = invalid; }

    ~Handle()
    {
        if (h != invalid)
        { ReleaseFunc{}(h); }
    }

    Handle &operator=(const Handle &other) = delete;
    Handle &operator=(Handle &&other) noexcept
    {
        this.h = other.h;
        other.h = invalid;
    }

    HandleType handle() const { return h; }

private:
    HandleType h;
};

#endif
