#ifndef QUAT_HPP_
#define QUAT_HPP_

#include <vec.hpp>

template <typename T>
class Quat
{
public:
    static_assert(std::numeric_limits<T>::is_iec559, "Floating point required.");

    Quat() : q{0, 0, 0, 1} {}
    Quat(const Vec4<T> &v) : q{v} {}
    Quat(const T x, const T y, const T z, const T w) : Quat{{x, y, z, w}} {}

    Quat<T> Unit() const
    { return {q.Unit()}; }

    Quat<T> Conj() const
    {
        Quat<T> newQ = -q;
        newQ.q.w() = q.w();
        return newQ;
    }

    Quat<T> operator*(const Quat<T> &other) const
    {
        return {
            w() * other.x() + other.w() * x() + y() * other.z() - other.y() * z(),
            w() * other.y() + other.w() * y() + z() * other.x() - other.z() * x(),
            w() * other.z() + other.w() * z() + x() * other.y() - other.x() * y(),
            w() * other.w() - x() * other.x() - y() * other.y() - z() * other.z()};
    }

    T x() const { return q.x(); }
    T y() const { return q.y(); }
    T z() const { return q.z(); }
    T w() const { return q.w(); }
private:
    Vec4<T> q;
};

#endif
