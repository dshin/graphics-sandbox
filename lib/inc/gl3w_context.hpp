#ifndef GL3W_CONTEXT_HPP_
#define GL3W_CONTEXT_HPP_

struct GL3WContext
{
    GL3WContext();
    GL3WContext(const unsigned major, const unsigned minor);
};

#endif
