#ifndef PLATFOR_GL_HPP_
#define PLATFOR_GL_HPP_

#if __has_include(<GL/gl3w.h>)
  #include <GL/gl3w.h>
#else
  #error "GL Platform header file not found."
#endif

#endif
