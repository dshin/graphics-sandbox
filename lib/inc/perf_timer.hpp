#ifndef PERF_TIMER_HPP_
#define PERF_TIMER_HPP_

#include <chrono>
#include <utility>

class Timer
{
public:
    using HRC = std::chrono::high_resolution_clock;
    using Duration = std::chrono::duration<double>;

    Timer();

    Duration elapsed() const;
    void Reset();

private:
    std::chrono::time_point<HRC> start;
};

class FPSCounter
{
public:
    FPSCounter(const double interval);

    std::pair<bool, double> update(const long unsigned tick, const double tickTime);

private:
    const double interval;
    double cumulatedTime;
    long unsigned startTick;
};

#endif
