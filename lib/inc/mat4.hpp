#ifndef MAT4_HPP_
#define MAT4_HPP_

#include <limits>
#include <initializer_list>

template <typename T>
class Mat4
{
public:
    static_assert(std::numeric_limits<T>::is_iec559, "Floating point required.");

    Mat4(const std::initializer_list<std::initializer_list <T>> &in) :
        arr{}
    {
        for (auto rowIt = in.begin(); rowIt != in.end(); ++rowIt)
        {
            const size_t row = rowIt - in.begin();
            if (row >= rows) { break; }

            for (auto colIt = rowIt->begin(); colIt != rowIt->end(); ++colIt)
            {
                const size_t col = colIt - rowIt->begin();
                if (col >= cols) { break; }

                arr[idx(row, col)] = *colIt;
            }
        }
    }

    Mat4() :
        arr{}
    {}

    static Mat4<T> Identity()
    {
        Mat4<T> mat{};

        for (unsigned row = 0; row < rows; row++)
        {
            for (unsigned col = 0; col < cols; col++)
            {
                if (row != col) { continue; }
                mat(row, col) = 1;
            }
        }

        return mat;
    }

    T &operator()(const size_t row, const size_t col)
    { return arr[idx(row, col)]; }

    T operator()(const size_t row, const size_t col) const
    { return arr[idx(row, col)]; }

    Mat4<T> operator*(const Mat4<T> &mat) const
    {
        Mat4<T> res;

        for (unsigned row = 0; row < rows; row++)
        {
            for (unsigned col = 0; col < cols; col++)
            {
                for (unsigned i = 0; i < rows; i++)
                {
                    res(row, col) += (*this)(row, i) * mat(i, col);
                }
            }
        }

        return res;
    }

    const T *data() const { return arr.data(); }

private:
    static size_t idx(const size_t row, const size_t col)
    { return row * rows + col; }

    static constexpr size_t rows = 4;
    static constexpr size_t cols = 4;
    static constexpr size_t numElements = rows * cols;

    std::array<T, numElements> arr;
};

#endif
