#ifndef FLOAT_EQ_HPP_
#define FLOAT_EQ_HPP_

#include <type_traits>
#include <cmath>

template <size_t N>
struct MatchingUnsigned {};

template<>
struct MatchingUnsigned<4>
{ using Type = uint32_t; };

template<>
struct MatchingUnsigned<8>
{ using Type = uint64_t; };

/*
 * Taken from gtest, and after consulting:
 * https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
 */
template <typename T>
bool FloatEquals(
    const T a,
    const T b,
    const T maxDiff = std::numeric_limits<T>::epsilon(), // Taken from a comment in the site above
    const unsigned maxUlpDiff = 4) // Taken from gtest
{
    static_assert(std::numeric_limits<T>::is_iec559, "Floating point required.");

    using Bits = typename MatchingUnsigned<sizeof(T)>::Type;
    constexpr size_t numBits = 8 * sizeof(T);
    constexpr Bits signBitMask = static_cast<Bits>(1) << (numBits - 1);

    union Float
    {
        T val;
        Bits bits;

        Float(const T v) : val{v} {}
    };

    /*
     * Lambda to shift sign + magnitude by adding half the max number of Bits type
     * so that positive and negative zeros sit next to each other.
     */
    auto shift = [] (const Bits v) -> Bits
    {
        if ((signBitMask & v) != 0)
        { return ~v + 1; }
        else
        { return signBitMask | v; }
    };

    // Lambda to calculate the (positive) distance between two values.
    auto distance = [] (const Bits a, const Bits b) -> Bits
    {
        if (a >= b) { return a - b; }
        return b - a;
    };

    // Short circuit on NaN.
    if (a != a || b != b) { return false; }

    const float absDiff = std::fabs(a - b);
    /*
     * Works well for values [1.0, 2.0]
     * Basically an exact equality check for > 2.0
     * Questionable for < 1.0, must tune maxDiff
     */
    if (absDiff <= maxDiff) { return true; }

    const Float ua{a};
    const Float ub{b};

    return distance(shift(ua.bits), shift(ub.bits)) < maxUlpDiff;
}

template <typename T>
struct FloatEqualsFuncDefault
{
    bool operator()(const T lhs, const T rhs) const
    { return FloatEquals(lhs, rhs); }
};

#endif
