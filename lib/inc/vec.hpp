#ifndef VEC_HPP_
#define VEC_HPP_

#include <array>
#include <cmath>
#include <type_traits>

template <typename T, size_t N>
class Vec
{
    template <typename, size_t> friend class Vec;
public:
    static_assert(std::numeric_limits<T>::is_iec559, "Floating point required.");
    static_assert(
        N >= 2 && N <= 4,
        "Only 2D, 3D and 4D vectors are supported.");
    Vec() : storage{} {}

    Vec(const T x, const T y) : storage{x, y}
    {
        if constexpr (N != 2)
        { static_assert(N == 2, "Attempting to call 2D ctor for non-2D vectors."); }
    }

    Vec(const T x, const T y, const T z) :
        storage{x, y, z}
    {
        if constexpr (N != 3)
        { static_assert(N == 3, "Attempting to call 3D ctor for non-3D vectors."); }
    }

    Vec(const T x, const T y, const T z, const T w) :
        storage{x, y, z, w}
    {
        if constexpr (N != 4)
        { static_assert(N == 4, "Attempting to call 4D ctor for non-4D vectors."); }
    }

    Vec<T, 4> ExtendTo4D() const
    {
        if constexpr (N == 4)
        { return *this; }
        else
        {
            Vec<T, 4> extended;
            for (unsigned i = 0; i < N; i++)
            { extended.storage[i] = storage[i]; }

            return extended;
        }
    }

    Vec<T, 3> ExtendTo3D() const
    {
        if constexpr (N == 4)
        { static_assert(N != 4, "+1 extension is not supported for 4D vectors."); }
        else if constexpr (N == 3)
        { return *this; }
        else
        {
            Vec<T, 3> extended;

            for (unsigned i = 0; i < N; i++)
            { extended.storage[i] = storage[i]; }
            return extended;
        }
    }

    T x() const { return storage[X]; }
    T &x() { return storage[X]; }

    T y() const { return storage[Y]; }
    T &y() { return storage[Y]; }

    T z() const
    {
        if constexpr (N < 3)
        { static_assert(N >= 3, "Attempting to access z from a 2D vector"); }
        return storage[Z];
    }

    T &z()
    {
        if constexpr (N < 3)
        { static_assert(N >= 3, "Attempting to access z from a 2D vector"); }
        return storage[Z];
    }

    T w() const
    {
        if constexpr (N < 4)
        { static_assert(N == 4, "Attempting to access w from a 2D/3D vector"); }
        return storage[W];
    }

    T &w()
    {
        if constexpr (N < 4)
        { static_assert(N == 4, "Attempting to access w from a 2D/3D vector"); }
        return storage[W];
    }

    Vec<T, N> operator-() const
    {
        Vec<T, N> res = *this;
        for (auto &v: res.storage)
        { v *= -1; }
        return res;
    }

    Vec<T, N> operator*(const T scalar) const
    {
        Vec<T, N> res;

        for (size_t i = 0; i < N; i++)
        { res.storage[i] = scalar * this->storage[i]; }

        return res;
    }

    Vec<T, N> operator/(const T scalar) const
    {
        Vec<T, N> res;

        for (size_t i = 0; i < N; i++)
        { res.storage[i] = this->storage[i] / scalar; }

        return res;
    }

    T Dot(const Vec<T, N> &other) const
    {
        T res = 0;

        for (size_t i = 0; i < N; i++)
        { res += other.storage[i] * this->storage[i]; }

        return res;
    }

    T Length2() const
    {
        T res = 0;

        for (const auto v: storage)
        { res += std::pow(v, 2); }

        return res;
    }

    T Length() const
    { return std::sqrt(Length2()); }

    Vec<T, N> Unit() const
    {
        const T length = Length();

        return *this / length;
    }

    Vec<T, N> Cross(const Vec<T, N> &other) const
    {
        if constexpr (N != 3)
        { static_assert(N == 3, "Cross product is only defined for 3D vectors."); }
        return {
            this->y() * other.z() - this->z() * other.y(),
            this->z() * other.x() - this->x() * other.z(),
            this->x() * other.y() - this->y() * other.x()};
    }

private:
    enum StorageIndex : size_t
    {
        X = 0,
        Y,
        Z,
        W
    };

    std::array<T, N> storage;
};

template <typename T, size_t N>
Vec<T, N> operator*(const T scalar, const Vec<T, N> &vec)
{ return vec * scalar; }

template<typename T>
using Vec2 = Vec<T, 2>;
template<typename T>
using Vec3 = Vec<T, 3>;
template<typename T>
using Vec4 = Vec<T, 4>;

#endif
