#include "shader.hpp"

#include <stdexcept>
#include <vector>

void ShaderDeleter::operator()(GLuint handle) const
{ glDeleteShader(handle); }

Shader::Shader(
    std::istream &stream,
    const GLenum type) :
    Handle{glCreateShader(type)}
{
    std::string str{std::istreambuf_iterator<char>(stream), {}};
    const GLchar *src = str.c_str();
    glShaderSource(handle(), 1, &src, 0);
    glCompileShader(handle());

    GLint compiled = GL_FALSE;
    glGetShaderiv(handle(), GL_COMPILE_STATUS, &compiled);
    if (compiled == GL_FALSE)
    {
		GLint logLength = 0;
		glGetShaderiv(handle(), GL_INFO_LOG_LENGTH, &logLength);

		std::vector<GLchar> infoLog(logLength);
		glGetShaderInfoLog(
			handle(),
            logLength,
            &logLength,
            &infoLog[0]);

        throw std::runtime_error(
            "Failed to compile: " +
            std::string(infoLog.begin(), infoLog.end()));
    }
}

