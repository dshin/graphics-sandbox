#include "perf_timer.hpp"

#include <chrono>

Timer::Timer() : start{HRC::now()} {}

Timer::Duration Timer::elapsed() const
{ return {HRC::now() - start}; }

void Timer::Reset()
{ *this = {}; }

FPSCounter::FPSCounter(const double interval) :
    interval{interval},
    cumulatedTime{0.0},
    startTick{0}
{
}

std::pair<bool, double> FPSCounter::update(const long unsigned tick, const double tickTime)
{
    cumulatedTime += tickTime;
    if (cumulatedTime >= interval)
    {
        const double elapsedTime = cumulatedTime;
        cumulatedTime = 0;
        // Beware, rollover
        const unsigned elapsedTicks = tick - startTick;
        startTick = tick;
        return {true, static_cast<double>(elapsedTicks) / elapsedTime};
    }
    return {false, 0.0};
}
