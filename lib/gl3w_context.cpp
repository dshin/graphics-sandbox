#include "gl3w_context.hpp"

#include <GL/gl3w.h>

#include <stdexcept>

GL3WContext::GL3WContext()
{
    int res = gl3wInit();
    if (res != 0)
    { throw std::runtime_error("Could not initialize gl3w: " + std::to_string(res)); }
}

GL3WContext::GL3WContext(const unsigned major, const unsigned minor) :
    GL3WContext{}
{
    if (!gl3wIsSupported(
        static_cast<int>(major),
        static_cast<int>(minor)))
    { throw std::runtime_error("Requested OpenGL version is not supported."); }
}
