#!/usr/bin/python

import argparse
import subprocess
import os
import errno

parser = argparse.ArgumentParser(description = 'Set up CMake environment for Linux w/ nVidia drivers.')
parser.add_argument('build_dir', help = 'Path to the build directory.')
parser.add_argument('--gl_dir', help = 'Directory to the GL library (For nVidia drivers on Ubuntu, probably /usr/lib/nvidia-<version>).')
parser.add_argument('--cc', help = 'C Compiler to use')
parser.add_argument('--cxx', help = 'C++ Compiler to use')

args = parser.parse_args()

repo_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')

try:
    os.makedirs(args.build_dir)
except OSError as e:
    if e.errno != errno.EEXIST:
        raise

os.chdir(args.build_dir)

new_env = os.environ.copy()
if args.gl_dir:
    new_env['CMAKE_LIBRARY_PATH'] = args.gl_dir

cmd = ['cmake', repo_dir]

if args.cc:
    cmd.append('-DCMAKE_C_COMPILER={}'.format(args.cc))
if args.cxx:
    cmd.append('-DCMAKE_CXX_COMPILER={}'.format(args.cxx))

subprocess.check_call(cmd, env = new_env)
