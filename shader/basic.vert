#version 330 core

uniform mat4 modelViewProjectionMatrix;
layout(location = 0) in vec3 model;

void main()
{
    gl_Position = modelViewProjectionMatrix * vec4(model, 1.0);
}
